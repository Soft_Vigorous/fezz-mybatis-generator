/**************************************************************************
 * Copyright (c) 2006-2018 ZheJiang Electronic Port, Inc.
 * All rights reserved.
 *
 * 项目名称：舱单与运输工具系统
 * 版权说明：本软件属浙江电子口岸有限公司所有，在未获得浙江电子口岸有限公司正式授权
 *           情况下，任何企业和个人，不能获取、阅读、安装、传播本软件涉及的任何受知
 *           识产权保护的内容。                            
 ***************************************************************************/
package com.fezz.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;

/**
 * TODO
 *
 * @author <a href="mailto:cjun@zjport.gov.cn">cjun</a>
 * @version $Id$
 * @since 1.0
 */
public class DemoTest {
    public static void main(String[] args) throws Exception {
        DemoVo demoVo = new DemoVo();
        demoVo.setName("rose");
        demoVo.setPassWord("123");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(demoVo);
        System.out.println(json);

    }
}