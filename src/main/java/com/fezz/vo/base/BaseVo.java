package com.fezz.vo.base;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by chiang on 2017/4/7.
 */
public class BaseVo implements Serializable {
    private Integer id;

    private Date createTime;

    private Date updateTime;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
