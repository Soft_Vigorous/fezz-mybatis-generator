/**************************************************************************
 * Copyright (c) 2006-2018 ZheJiang Electronic Port, Inc.
 * All rights reserved.
 *
 * 项目名称：舱单与运输工具申报系统
 * 版权说明：本软件属浙江电子口岸有限公司所有，在未获得浙江电子口岸有限公司正式授权
 *           情况下，任何企业和个人，不能获取、阅读、安装、传播本软件涉及的任何受知
 *           识产权保护的内容。
 ***************************************************************************/
package com.fezz.vo;

import com.fezz.base.BaseVo;
import java.util.Date;

/**
 * desc TODO
 *
 * @author <a href="mailto:cjun@zjport.gov.cn">cjun</a>
 * @version $Id$
 * @since 1.0
 */
public class User extends BaseVo {
    /** 主键 */
    private Integer id;

    /** 用户名称 */
    private String userName;

    /** 姓名 */
    private String name;

    /** 密码 */
    private String password;

    /** 角色ID */
    private Integer roleId;

    /** 备注 */
    private String remark;

    /** 删除标志（0-未删除，1-已删除） */
    private String deletedFlag;

    /** 创建人 */
    private Integer createId;

    /** 修改人 */
    private Integer modifyId;

    /** 修改时间 */
    private Date modifyTime;

    /**
     * @return the value of id
    */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the value for id
    */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the value of user_name
    */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the value for user_name
    */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the value of name
    */
    public String getName() {
        return name;
    }

    /**
     * @param name the value for name
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of password
    */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the value for password
    */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the value of role_id
    */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the value for role_id
    */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the value of remark
    */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the value for remark
    */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the value of deleted_flag
    */
    public String getDeletedFlag() {
        return deletedFlag;
    }

    /**
     * @param deletedFlag the value for deleted_flag
    */
    public void setDeletedFlag(String deletedFlag) {
        this.deletedFlag = deletedFlag;
    }

    /**
     * @return the value of create_id
    */
    public Integer getCreateId() {
        return createId;
    }

    /**
     * @param createId the value for create_id
    */
    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    /**
     * @return the value of modify_id
    */
    public Integer getModifyId() {
        return modifyId;
    }

    /**
     * @param modifyId the value for modify_id
    */
    public void setModifyId(Integer modifyId) {
        this.modifyId = modifyId;
    }

    /**
     * @return the value of modify_time
    */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * @param modifyTime the value for modify_time
    */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}