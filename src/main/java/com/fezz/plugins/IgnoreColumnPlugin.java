package com.fezz.plugins;

import com.fezz.constant.Constant;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.Plugin;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;
import java.util.Set;

/**
 * 字段忽略插件
 *
 * @author <a href="mailto:fezz_ggjp@163.com">fezz</a>
 * @version $Id$
 * @since 1.0
 */
public class IgnoreColumnPlugin extends PluginAdapter {
    /**
     * This method is called after all the setXXX methods are called, but before
     * any other method is called. This allows the plugin to determine whether
     * it can run or not. For example, if the plugin requires certain properties
     * to be set, and the properties are not set, then the plugin is invalid and
     * will not run.
     *
     * @param warnings add strings to this list to specify warnings. For example, if
     *                 the plugin is invalid, you should specify why. Warnings are
     *                 reported to users after the completion of the run.
     * @return true if the plugin is in a valid state. Invalid plugins will not
     * be called
     */
    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
        String name = field.getName();
        System.out.println("field name = " + name);
        /*if ((Constant.CREATE_ID_ATTR.equals(name))
                || (Constant.UPDATE_ID_ATTR.equals(name)) || (Constant.CREATE_TIME_ATTR.equals(name))
                || (Constant.UPDATE_TIME_ATTR.equals(name)) || Constant.DELETED_FLAG_ATTR.equals(name)
                || Constant.REMARK_ATTR_OR_COLUMN.equals(name)) {
            return false;
        }*/
        if (Constant.CREATE_TIME_ATTR.equals(name) || Constant.UPDATE_TIME_ATTR.equals(name)
                || Constant.CREATE_TIME_ATTR_2.equals(name) || Constant.UPDATE_TIME_ATTR_2.equals(name)) {
            Set<FullyQualifiedJavaType> importedTypes = topLevelClass.getImportedTypes();
            FullyQualifiedJavaType fullyQualifiedJavaType = new FullyQualifiedJavaType("import org.springframework.format.annotation.DateTimeFormat");
            if (!importedTypes.contains(fullyQualifiedJavaType)){
                topLevelClass.addImportedType(fullyQualifiedJavaType);
            }
        }
        return super.modelFieldGenerated(field, topLevelClass, introspectedColumn, introspectedTable, modelClassType);
    }

    @Override
    public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
        /*String actualColumnName = introspectedColumn.getActualColumnName();
        actualColumnName = actualColumnName.toLowerCase();
        if ((Constant.CREATE_ID_COLUMN.equals(actualColumnName))
                || (Constant.UPDATE_ID_COLUMN.equals(actualColumnName)) || (Constant.CREATE_TIME_COLUMN.equals(actualColumnName))
                || (Constant.UPDATE_TIME_COLUMN.equals(actualColumnName)) || Constant.DELETED_FLAG_COLUMN.equals(actualColumnName)
                || Constant.REMARK_ATTR_OR_COLUMN.equals(actualColumnName)) {
            return false;
        }*/
        return super.modelSetterMethodGenerated(method, topLevelClass, introspectedColumn, introspectedTable, modelClassType);
    }

    @Override
    public boolean modelGetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
        /*String actualColumnName = introspectedColumn.getActualColumnName();
        actualColumnName = actualColumnName.toLowerCase();
        if ((Constant.CREATE_ID_COLUMN.equals(actualColumnName))
                || (Constant.UPDATE_ID_COLUMN.equals(actualColumnName)) || (Constant.CREATE_TIME_COLUMN.equals(actualColumnName))
                || (Constant.UPDATE_TIME_COLUMN.equals(actualColumnName)) || Constant.DELETED_FLAG_COLUMN.equals(actualColumnName)
                || Constant.REMARK_ATTR_OR_COLUMN.equals(actualColumnName)) {
            return false;
        }*/
        return super.modelGetterMethodGenerated(method, topLevelClass, introspectedColumn, introspectedTable, modelClassType);
    }
}
