package com.fezz.plugins;

import org.apache.log4j.Logger;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.XmlElement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * ${desc}
 *
 * @author <a href="mailto:cjun@zjport.gov.cn">cjun</a>
 * @version $Id$
 * @since 1.0
 */
public class BaseMapperPlugin extends PluginAdapter {

    private static final Logger logger = Logger.getLogger(BaseMapperPlugin.class);

    /**
     * This method is called after all the setXXX methods are called, but before
     * any other method is called. This allows the plugin to determine whether
     * it can run or not. For example, if the plugin requires certain properties
     * to be set, and the properties are not set, then the plugin is invalid and
     * will not run.
     *
     * @param warnings add strings to this list to specify warnings. For example, if
     *                 the plugin is invalid, you should specify why. Warnings are
     *                 reported to users after the completion of the run.
     * @return true if the plugin is in a valid state. Invalid plugins will not
     * be called
     */
    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public void setProperties(Properties properties) {
        super.setProperties(properties);
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        System.out.println("*************************clientGenerated******************");
        String baseMapper = this.properties.getProperty("baseMapper");
        interfaze.addImportedType(new FullyQualifiedJavaType(baseMapper));
        FullyQualifiedJavaType type = interfaze.getType();
        System.out.println("type = " + type);
        Set<FullyQualifiedJavaType> importedTypes = interfaze.getImportedTypes();
        System.out.println("*************************importedTypes******************");
        for (FullyQualifiedJavaType importedType : importedTypes) {
            String packageName = importedType.getPackageName();
            System.out.println("packageName = " + packageName);
            String shortName = importedType.getShortName();
            System.out.println("shortName = " + shortName);
            if (shortName != null && !"".equals(shortName)) {
                shortName = shortName.substring(shortName.length() - 2).equals("DO") ? shortName.substring(0, shortName.length() - 2) : shortName;
            }
            if (packageName.indexOf("falconeyes") > 0 || packageName.indexOf("entity") > 0 || packageName.indexOf("domain") > 0 || packageName.indexOf("model") > 0) {
                FullyQualifiedJavaType baseMapperGeneric = new FullyQualifiedJavaType("Mapper<" + importedType.getShortName() + ">");
                interfaze.addJavaDocLine("/**");
                interfaze.addJavaDocLine(" * " + shortName + "Mapper");
                interfaze.addJavaDocLine(" *");
                interfaze.addJavaDocLine(" * @author muyun");
                interfaze.addJavaDocLine(" * @since " + new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
                interfaze.addJavaDocLine(" */");
                interfaze.addSuperInterface(baseMapperGeneric);
            }
        }
        List methods = interfaze.getMethods();
        methods.clear();
        return super.clientGenerated(interfaze, topLevelClass, introspectedTable);
    }

    @Override
    public boolean sqlMapDeleteByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        // System.out.println("sqlMapDeleteByPrimaryKeyElementGenerated --> deleteById -----start----");
        // Attribute a = getIdAttr(element);
        // System.out.println("a --> " + a.getName());
        // element.getAttributes().remove(a);
        // element.addAttribute(new Attribute("id", "deleteById"));
        // return super.sqlMapDeleteByPrimaryKeyElementGenerated(element, introspectedTable);
        return false;
    }

    @Override
    public boolean sqlMapSelectByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        /*System.out.println("sqlMapSelectByPrimaryKeyElementGenerated --> selectById -----start----");
        Attribute a = getIdAttr(element);
        System.out.println("a --> " + a.getName());
        element.getAttributes().remove(a);
        element.addAttribute(new Attribute("id", "selectById"));
        return super.sqlMapSelectByPrimaryKeyElementGenerated(element, introspectedTable);*/
        return false;
    }

    private Attribute getIdAttr(XmlElement element) {
        List<Attribute> attributes = element.getAttributes();
        for (Attribute a : attributes) {
            if ("id".equals(a.getName())) {
                attributes.remove(a);
                return a;
            }
        }
        return null;
    }

    @Override
    public boolean sqlMapUpdateByPrimaryKeySelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        /*System.out.println("sqlMapUpdateByPrimaryKeySelectiveElementGenerated --> updateById -----start----");
        Attribute a = getIdAttr(element);
        System.out.println("a --> " + a.getName() + ",getValue = " + a.getValue());
        if ("updateByPrimaryKeySelective".equals(a.getValue())) {
            element.getAttributes().remove(a);
            element.addAttribute(new Attribute("id", "updateById"));
        } else {
            return false;
        }

        return super.sqlMapUpdateByPrimaryKeySelectiveElementGenerated(element, introspectedTable);*/
        return false;
    }

    @Override
    public boolean sqlMapUpdateByPrimaryKeyWithBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        return false;
    }

    @Override
    public boolean sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        return false;
    }

    @Override
    public boolean sqlMapInsertSelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        /*System.out.println("sqlMapUpdateByPrimaryKeySelectiveElementGenerated --> updateById -----start----");
        Attribute a = getIdAttr(element);
        System.out.println("a --> " + a.getName());
        element.getAttributes().remove(a);
        element.addAttribute(new Attribute("id", "insert"));

        *//*mysql主键策略*//*
        element.addAttribute(new Attribute("useGeneratedKeys", "true"));
        element.addAttribute(new Attribute("keyProperty", "id"));
        element.addAttribute(new Attribute("keyColumn", "id"));
        return super.sqlMapInsertSelectiveElementGenerated(element, introspectedTable);*/
        return false;
    }

    @Override
    public boolean sqlMapInsertElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        return false;
    }
}
