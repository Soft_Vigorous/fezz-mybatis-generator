package com.fezz.base;

import java.io.Serializable;

public interface BaseMapper<E extends BaseVo> {

    int deleteById(Serializable id);

    int insert(E record);

    E selectById(Serializable id);

    int updateById(E record);

}
