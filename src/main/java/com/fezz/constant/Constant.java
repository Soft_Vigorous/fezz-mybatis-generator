package com.fezz.constant;

/**
 * 常量
 *
 * @author <a href="mailto:fezz_ggjp@163.com">fezz</a>
 * @version $Id$
 * @since 1.0
 */
public class Constant {

    /** 主键 */
    // public static final String ID_ATTR_OR_COLUMN = "id";
    /** 属性：createId */
    public static final String CREATE_ID_ATTR = "createdby";
    /** 字段：create_id */
    public static final String CREATE_ID_COLUMN = "createdby";
    /** 属性：updateId */
    public static final String UPDATE_ID_ATTR = "modifiedby";
    /** 字段：update_id */
    public static final String UPDATE_ID_COLUMN = "modifiedby";
    /** 创建时间实体类中属性：createTime */
    public static final String CREATE_TIME_ATTR = "createdon";
    /** 创建时间实体类中属性：createTime */
    public static final String CREATE_TIME_ATTR_2 = "createTime";
    /** 创建时间表中字段名：create_time */
    public static final String CREATE_TIME_COLUMN = "createdon";
    /** 修改时间实体类中属性：updateTime */
    public static final String UPDATE_TIME_ATTR = "modifiedon";
    /** 修改时间实体类中属性：updateTime */
    public static final String UPDATE_TIME_ATTR_2 = "modifyTime";
    /** 修改时间表中字段名：update_time */
    public static final String UPDATE_TIME_COLUMN = "modifiedon";
    /** 备注 */
    public static final String REMARK_ATTR_OR_COLUMN = "remark";
    /** 删除标志实体类中属性：deletedFlag */
    public static final String DELETED_FLAG_ATTR = "deleted";
    /** 删除标志表中字段名：deleted_flag */
    public static final String DELETED_FLAG_COLUMN = "deleted";
}
